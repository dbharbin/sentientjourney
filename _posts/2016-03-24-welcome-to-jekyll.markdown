---
layout: post
title:  "Welcome to LxZone"
date:   2016-03-24 15:32:14 -0300
categories: jekyll update
---
Welcome to LxZone,

This is a very basic POC that shows the following Hosted GitLab Pages feature:
* Auto-generated static site leveaging GitLab Pages.
* Domain name redirection from providers, in this case the domain name is managed from Go-Daddy.
* Basic look feel and how the final site will redirect to GitLab projects. Click to see the [Projects link](http://sentientjourney.com/projects.html) for an example.


<br/>
<br/>
<br/>
====
Jekyll also offers powerful support for code snippets:

```
{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}
```

Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can ask them on [Jekyll Talk][jekyll-talk].

[jekyll-docs]: http://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
